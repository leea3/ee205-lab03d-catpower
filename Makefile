###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 03d - Cat Power - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Makefile for  Cat Power Lab
###
### @author  Arthur Lee <leea3@hawaii.edu>
### @date    2 Feb 2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

#gcc for C, g++ for C++
CC     = g++
CFLAGS = -g -Wall

TARGET = catPower

all: $(TARGET)

#.c for C, .cpp for C++
catPower: catPower.cpp
	$(CC) $(CFLAGS) -o $(TARGET) catPower.cpp

clean:
	rm -f $(TARGET) *.o

