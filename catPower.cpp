///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file catPower.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Arthur Lee <leea3@hawaii.edu>
/// @date   2 Feb 2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

// A Joule represents the amount of electricity required to run a 1 W device for 1 s

const double ELECTRON_VOLTS_IN_A_JOULE = 6.24150974e18;
const double MEGATONS_IN_A_JOULE = 1 / (4.184e15);
const double ONE_GALLON_GASOLINE_IN_A_JOULE = 1 / (1.213e8);
const double FOE_IN_A_JOULE = 1 / (1e24);
const double CATPOWER_IN_A_JOULE = 0; //cats do absolutely no work at all...

const char JOULE         = 'j';
const char ELECTRON_VOLT = 'e';
const char MEGATON = 'm';
const char ONE_GALLON_GASOLINE = 'g';
const char FOE = 'f';
const char CAT_POWER = 'c';


double fromElectronVoltsToJoule( double electronVolts ) {
   return electronVolts / ELECTRON_VOLTS_IN_A_JOULE ;
}


double fromJouleToElectronVolts( double joule ) {
   return joule * ELECTRON_VOLTS_IN_A_JOULE;
}


double fromMegatonsToJoule( double megaton ) {
   return megaton / MEGATONS_IN_A_JOULE;
}

double fromJouleToMegatons( double joule ) {
   return joule * MEGATONS_IN_A_JOULE;
}

double fromGallonsOfGasolineToJoule( double gasoline ) {
   return gasoline / ONE_GALLON_GASOLINE_IN_A_JOULE;
}

double fromJouleToGallonsOfGasoline( double joule ) {
   return joule * ONE_GALLON_GASOLINE_IN_A_JOULE;
}

double fromFoeToJoule( double foe ) {
   return foe / FOE_IN_A_JOULE;
}

double fromJouleToFoe( double joule ) {
   return joule * FOE_IN_A_JOULE;
}

double fromCatPowerToJoule( double catPower ) {
   return 0.0;  // Cats do no work
}

double fromJouleToCatPower( double joule ) {
   return 0.0; // Infinite cats?
}




int main( int argc, char* argv[] ) {
   printf( "Energy converter\n" );

   printf( "Usage:  catPower fromValue fromUnit toUnit\n" );
   printf( "   fromValue: A number that we want to convert\n" );
   printf( "   fromUnit:  The energy unit fromValue is in\n" );
   printf( "   toUnit:  The energy unit to convert to\n" );
   printf( "\n" );
   printf( "This program converts energy from one energy unit to another.\n" );
   printf( "The units it can convert are: \n" );
   printf( "   j = Joule\n" );
   printf( "   e = eV = electronVolt\n" );
   printf( "   m = MT = megaton of TNT\n" );
   printf( "   g = GGE = gasoline gallon equivalent\n" );
   printf( "   f = foe = the amount of energy produced by a supernova\n" );
   printf( "   c = catPower = like horsePower, but for cats\n" );
   printf( "\n" );
   printf( "To convert from one energy unit to another, enter a number \n" );
   printf( "it's unit and then a unit to convert it to.  For example, to\n" );
   printf( "convert 100 Joules to GGE, you'd enter:  $ catPower 100 j g\n" );
   printf( "\n" );

   double fromValue;
   char   fromUnit;
   char   toUnit;

   fromValue = atof( argv[1] );
   fromUnit = argv[2][0];         // Get the first character from the second argument
   toUnit = argv[3][0];           // Get the first character from the thrid argument

   printf( "fromValue = [%lG]\n", fromValue );   /// @todo Remove before flight
   printf( "fromUnit = [%c]\n", fromUnit );      /// @todo Remove before flight
   printf( "toUnit = [%c]\n", toUnit );          /// @todo Remove before flight

   double commonValue;
   switch( fromUnit ) {
      case JOULE                 : commonValue = fromValue; // No conversion necessary
                                   break;
      case ELECTRON_VOLT         : commonValue = fromElectronVoltsToJoule( fromValue );
                                   break;
      case MEGATON               : commonValue = fromMegatonsToJoule( fromValue );
                                   break;
      case ONE_GALLON_GASOLINE   : commonValue = fromGallonsOfGasolineToJoule( fromValue);
                                   break;
      case FOE                   : commonValue = fromFoeToJoule( fromValue );
                                   break;
      case CAT_POWER             : commonValue = fromCatPowerToJoule( fromValue );;
                                   break;
      default                    : printf("Invalid fromUnit, enter a valid one\n");
                                   return 1;
                                   break;
   }

   printf( "commonValue = [%lG] joule\n", commonValue ); /// @todo Remove before flight


	/// @todo Write a switch statement to convert common units to toUnit.  
	///       Don't forget to break; after every case.
   double toValue;

   switch( toUnit ) {
      case JOULE                 : toValue = commonValue; // no conversion needed
                                   break;
      case ELECTRON_VOLT         : toValue = fromJouleToElectronVolts( commonValue );
                                   break;
      case MEGATON               : toValue = fromJouleToMegatons( commonValue );
                                   break;
      case ONE_GALLON_GASOLINE   : toValue = fromJouleToGallonsOfGasoline( commonValue );
                                   break;
      case FOE                   : toValue = fromJouleToFoe( commonValue );
                                   break;
      case CAT_POWER             : toValue = fromJouleToCatPower( commonValue );
                                   break;
      default                    : printf("invalid toUnit, enter a valid one\n");
                                   return 1;
                                   break;
   }

   
   printf( "%lG %c is %lG %c\n", fromValue, fromUnit, toValue, toUnit );
}
